(() => {
	var popupProcesses = {
		addEnablerBtn: () => {
			var enabler = document.createElement('input');
			chrome.popupProcesses.getUserAgent().then((response) => {
				uagent.innerText = response;
				chrome.popupProcesses.messageTab({
					'PopupOpenWebResponse': response
				});
				document.body.style.paddingBottom = 0;
				enabler.value = (/OPR/.test(response)) ? 'Enable Agent' : 'Disable Agent';
				if(/OPR/.test(response)) {
					enabler.value = 'Enable Agent';
					chrome.storage.local.get('ChromeAgent', (store) => {
						var obj = store;
						if(obj.ChromeAgent.AgentState === 'Enabled') {
							obj.ChromeAgent.AgentState = 'Disabled';
						}
						chrome.storage.local.set(obj);
					});
				} else {
					enabler.value = 'Disable Agent';
					chrome.storage.local.get('ChromeAgent', (store) => {
						var obj = store;
						if(obj.ChromeAgent.AgentState === 'Disabled') {
							obj.ChromeAgent.AgentState = 'Enabled';
						}
						chrome.storage.local.set(obj);
					});
				}
			});
			enabler.id = "enabler";
			enabler.type = "button"
			enabler.classList.add('button');
			enabler.classList.add('controller');
			enabler.setAttribute('data-handler', 'toggle-dev-on');
			enabler.style.marginBottom = '3px';
			enabler.onclick = function(evt) {
				//evt.srcElement.value = (evt.srcElement.value === "Enable Agent") ? "Disable Agent" : "Enable Agent";
				chrome.storage.local.get('ChromeAgent', (items) => {
					var obj = items;
					var evtValue;
					if(obj.ChromeAgent.AgentState === "Enabled") {
						obj.ChromeAgent.AgentState = 'Disabled';
						chrome.popupProcesses.messageTab('Here 1');
						evtValue = 'Enable Agent';
					} else if(obj.ChromeAgent.AgentState === "Disabled") {
						obj.ChromeAgent.AgentState = 'Enabled';
						chrome.popupProcesses.messageTab('Here 2');
						evtValue = 'Disable Agent';
					}
					chromep.storage.local.set(obj).then(() => {
						chrome.popupProcesses.getUserAgent().then((response) => {
							uagent.innerText = response;
							chrome.popupProcesses.messageTab({
								'GetUserAgentResponse': response
							});
							document.body.style.paddingBottom = 0;
							chrome.uavalue = items.ChromeAgent.UserAgent;
							window.uavalue = items.ChromeAgent.UserAgent;
							chrome.popupProcesses.messageTab({
								'StorageObj': obj.ChromeAgent.AgentState
							});
							evt.srcElement.value = evtValue;
						});
					});
				});
				/*
				chrome.storage.local.get('ChromeAgentStatus', (stat) => {
					if(stat.ChromeAgentStatus === "Enabled") {
						chrome.storage.local.set({
							'ChromeAgentStatus': 'Disabled'
						}, function() {
							//document.querySelector('#show-agent').click();
							chrome.popupProcesses.getUserAgent();
							chrome.popupProcesses.dosomeshit(stat);
						});
					}
					if(stat.ChromeAgentStatus === "Disabled") {
						chrome.storage.local.set({
							'ChromeAgentStatus': 'Enabled'
						}, function() {
							//document.querySelector('#show-agent').click();
							chrome.popupProcesses.getUserAgent();
							chrome.popupProcesses.dosomeshit(stat);
						});
					}
				});
				*/

			};
			var buttons = document.querySelector('#buttons');
			buttons.appendChild(enabler);
			//chrome.popupProcesses.getUserAgent();
		},
		addUserAgentBtn: () => {
			var sua = document.createElement('input');
			sua.value = 'Show User Agent';
			sua.type = "button";
			sua.id = 'show-agent';
			sua.classList.add('button');
			sua.classList.add('controller');
			sua.setAttribute('data-handler', 'toggle-dev-on');
			//sua.style.marginBottom = '3px';
			sua.onclick = () => chrome.popupProcesses.getUserAgent();
			var buttons = document.querySelector('#buttons');
			//buttons.appendChild(sua);
		},
		addOptionsButton: () => {
			var optsBtn = document.createElement('input');
			optsBtn.value = 'Options';
			optsBtn.type = 'button';
			optsBtn.id = 'opts-btn';
			optsBtn.classList.add('button');
			optsBtn.classList.add('controller');
			optsBtn.setAttribute('data-handler', 'toggle-dev-on');
			//optsBtn.style.marginBottom = '3px';
			optsBtn.onclick = () => {
				chrome.tabs.create({
					url: "/html/options.html"
				});
			}
			var buttons = document.querySelector('#buttons');
			buttons.appendChild(optsBtn);
		},
		getUserAgent: () => {
			return new Promise((resolve) => {	
				//var oReq = new XMLHttpRequest();
				//oReq.open("GET", 'https://www.whoishostingthis.com/tools/user-agent/');
				//oReq.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				//oReq.onload = function() {
				//	//resolve(unescape(this.responseText));
				//	var data = this.responseText.replace(/[\w\W]*<div class="info-box user-agent">([\w\W]*?)<\/div>[\w\W]*/g, '$1').replace(/<.*?>/g, '');
				//	var browsertype, browserversion;
				//	chrome.popupProcesses.messageTab({
				//		'getUserAgent': data
				//	});
				//	if(/OPR/.test(data)) {
				//		browsertype = 'Opera';
				//		browserversion = data.match(/OPR\/([^\s]+)/)[1];
				//		var output = 'Browser: ' + browsertype + '\n' + 'Version: ' + browserversion + '\n' + 'User Agent:\n' + data;
				//		resolve(output);
				//	}
				//	else {
				//		chrome.storage.local.get('ChromeAgent', (store) => {
				//			browsertype = store.ChromeAgent.UserAgentType;
				//			browserversion = data.substr(data.lastIndexOf('/')+1,data.length);
				//			var output = 'Browser: ' + browsertype + '\n' + 'Version: ' + browserversion + '\n' + 'User Agent:\n' + data;
				//			resolve(output);
				//		});
				//	}
				//};
				//oReq.send();
				chrome.storage.local.get('ChromeAgent', (store) => {
					var output;
					if(store.ChromeAgent.AgentState === 'Enabled') {
						output = 'Browser: ' + store.ChromeAgent.UserAgentType + '\n' + 'User Agent:\n' + store.ChromeAgent.UserAgent;
					}
					else {
						output = 'Browser: Opera\n' + 'User Agent:\n' + window.navigator.userAgent;
					}
					resolve(output);
				});
			});
		},
		messageTab: (status) => {
			chrome.tabs.query({
				"active": true,
				"lastFocusedWindow": true
			}, (tabs) => {
				//if(tabs[0].url !== chrome.extension.getURL('')) {
				console.log({
					'tabs': tabs[0]
				});
				chrome.tabs.executeScript(tabs[0].id, {
					code: `console.log(${JSON.stringify(status)});`
				}, (result) => {
					console.log(result);
				});
				//}
			});
		},
		buildPopup: () => {
			chrome.popupProcesses.addEnablerBtn();
			chrome.popupProcesses.addOptionsButton();
			chrome.popupProcesses.addUserAgentBtn();
		}
	}
	chrome.popupProcesses = popupProcesses;
})();

document.addEventListener('DOMContentLoaded', chrome.popupProcesses.buildPopup);