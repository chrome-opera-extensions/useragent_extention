document.querySelector('#uagent').innerText = platform.ua;

((root, factory) => {
	var dothis = function() {
		return {
			msg: () => console.log("Doing this.")
		}
	}
	root.dothis = dothis;
	var script = document.currentScript;
	if(script) {
		var name = script.dataset.instance;
		if(name) {
			root[name] = new root.dothis();
		}
	}
})(typeof self !== 'undefined' ? self : this);