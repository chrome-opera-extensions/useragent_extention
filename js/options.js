var buildChromeAgentStore = (useragents) => {
	return new Promise(resolve => {
		chrome.storage.local.get('ChromeState', (items) => {
			var d = new Date();
			var DateTime = {
				date: d.toLocaleDateString(),
				time: d.toLocaleTimeString()
			};

			var obj,
				agentState,
				userAgent = {};
				userAgent.type = 'Chrome;'
				userAgent.agent = 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36';
			if(!!items.ChromeAgent) {
				if(!!items.ChromeAgent.AgentState) {
					agentState = items.ChromeAgent.AgentState
				} else {
					agentState = 'Disabled';
				}
				if(!!items.ChromeAgent.UserAgent) {
					userAgent.agent = items.ChromeAgent.UserAgent;
				}
				if(!!items.ChromeAgent.UserAgentType) {
					userAgent.type = items.Chrome.UserAgentType;
				}
				obj = {
					ChromeAgent: {
						AgentList: useragents,
						AgentState: agentState,
						AgentListDate: DateTime,
						UserAgentType: 	userAgent.type,
						UserAgent: userAgent.agent
					}
				}
			} else {
				obj = {
					ChromeAgent: {
						AgentList: useragents,
						AgentState: 'Disabled',
						AgentListDate: DateTime,
						UserAgentType: 	userAgent.type,
						UserAgent: userAgent.agent
					}
				}
			}
			resolve(obj);
		});
	});
}

var getUserAgentList = (ajaxBool) => {
	return new Promise((resolve) => {
		if(ajaxBool) {
			console.log('Getting user agent list from web');
			var oReq = new XMLHttpRequest();
			oReq.open("GET", 'https://deviceatlas.com/blog/list-of-user-agent-strings');
			oReq.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			var useragents = [];
			oReq.onload = function() {
				var data = this.responseText;
				var regd = data.match(/(<th>(.*?)<\/th>|<td>(.*?)<\/td>)/gmi);
				window.regd = regd;
				for(let i = 0; i < regd.length; i++) {
					var title, agent;
					if(i % 2 == 0) {
						title = regd[i].replace('<th>', '').replace('</th>', '');
						if(/([^<]+)</.test(title)) {
							title = /([^<]+)</.exec(title)[1];
						}
						agent = regd[i + 1].replace('<td>', '').replace('</td>', '');
						if(/([\s\S]+)<a name="\w+"><\/a>/.test(agent)) {
							agent = /([\s\S]+)<a name="\w+"><\/a>/.exec(agent)[1];
						}
					}
					i++
					useragents.push({
						'title': title,
						'agent': agent
					});
				}
				//resolve(useragents);
				buildChromeAgentStore(useragents).then((obj) => {
					chromep.storage.local.set(obj).then(function() {
						return chromep.storage.local.get('ChromeAgent');
					}).then(function(response) {
						resolve(response);
					});
				});
			}
			oReq.send(null);
		} else {
			console.log('Getting user agent list from storage');
			chrome.storage.local.get('ChromeAgent', function(e) {
				if(!!e.ChromeAgent) {
					resolve(e);
				}
			});
		}
	});
}

var updateAgentList = (ajaxBool) => {
	getUserAgentList(ajaxBool).then((items) => {
		var ChromeAgent = items.ChromeAgent;
		var agents = ChromeAgent.AgentList;
		var DateTime = ChromeAgent.AgentListDate;
		window.platforms = agents;
		if(ajaxBool) {
			document.getElementById("config").innerHTML = '';
		}
		for(let i = 0; i < agents.length; i++) {
			let item = agents[i];
			let title = document.createElement('div');
			title.style.fontWeight = 'bold';
			title.style.textDecoration = 'underline';
			title.style.backgroundColor = 'white';
			title.innerText = item.title;
			var ckbox = document.createElement('input');
			ckbox.type = 'checkbox';
			ckbox.name = 'checkbox';
			ckbox.id = `ua-cbox-${i}`;
			ckbox.onclick = function(evt) {
				selectOnlyThis(evt)
			};
			var agent = document.createElement('div');
			agent.style.backgroundColor = 'white';
			agent.appendChild(ckbox);
			var uainfo = document.createElement('span');
			uainfo.id = `ua-info-${i}`;
			uainfo.innerText = item.agent;
			uainfo.style.marginLeft = '5px';
			agent.appendChild(uainfo);
			agent.style.marginLeft = '15px';
			document.getElementById("config").appendChild(title);
			document.getElementById("config").appendChild(agent);
		}
		document.querySelector('#date-time').innerHTML = DateTime.date + ', ' + DateTime.time;
	});
	document.querySelector('#config').style.height = (document.body.offsetHeight - '77') + 'px';
}

document.body.onresize = function() {
	document.querySelector('#config').style.height = (document.body.offsetHeight - '77') + 'px';
}

function selectOnlyThis(evt) {
	var id = evt.target.id;
	console.log(id);
	document.querySelectorAll('input[type="checkbox"]').forEach(ckbox => {
		if(ckbox.id !== id) {
			document.getElementById(ckbox.id).checked = false;
		}
	});
	chgagent.disabled = (evt.target.checked) ? false : true;
}

chgagent.onclick = function() {
	document.querySelectorAll('input[type="checkbox"]').forEach(ckbox => {
		if(ckbox.checked) {
			var id = ckbox.id.substr(ckbox.id.lastIndexOf('-') + 1, ckbox.id.length);
			id = '#ua-info-' + id;
			console.log(document.querySelector(id).innerText);
			var agent;
			chrome.storage.local.get('ChromeAgent', function(e) {
				if(e.ChromeAgent) {
					var obj = e;
					obj.ChromeAgent.UserAgentType = document.querySelector(id).parentElement.previousElementSibling.innerText;
					obj.ChromeAgent.UserAgent = document.querySelector(id).innerText;
					console.log(obj);
					chrome.storage.local.set(obj);
					ckbox.checked = false;
					chgagent.disabled = true;
					alert('New User Agent:\n' + obj.ChromeAgent.UserAgent);
				}
			});

		}
	});
}

updatelist.onclick = () => {
	updateAgentList(true);
}

updateAgentList(false);