var chromeAgent = {
	Id: "ChromeWin",
	Name: "Chrome on Windows",
	UserAgent: "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"
}
var operaAgent = {
	Id: "OperaAgent",
	Name: "Opera on Windows",
	UserAgent: "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36 OPR/56.0.3051.52"
}

var handler = function(n) {
	theUserAgent = chrome.uavalue || window.uavalue;
	//theUserAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246';
	//var chromeAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
	//var operaAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36 OPR/56.0.3051.52";
	for(var i = 0; i < n.requestHeaders.length; ++i) {
		if(n.requestHeaders[i].name === 'User-Agent') {
			n.requestHeaders[i].value = (window.chromevalue === 'Enabled') ? theUserAgent : navigator.userAgent;
			window.uagent = n.requestHeaders[i];
			break;
		}
	}
	chrome.navagent = n;
	return {
		requestHeaders: n.requestHeaders
	};
};

function requestProcessor(details) {
	window.receivedHeader = details;
}

chrome.webRequest.onBeforeSendHeaders.addListener((handler), {
	urls: ["<all_urls>"]
}, ["blocking", "requestHeaders"]);

//chrome.webRequest.onHeadersReceived.addListener(requestProcessor, {
//    urls: ["*://*/*"],
//    types: ["main_frame", "sub_frame"]
//}, ["blocking", "responseHeaders"]);

var genericChromeAgentStore = () => {
	var d = new Date();
	var DateTime = {
		date: d.toLocaleDateString(),
		time: d.toLocaleTimeString()
	};
	var obj = {
		ChromeAgent: {
			AgentList: [{'title':'Chrome Agent', 'agent': chromeAgent.UserAgent}],
			AgentListDate: DateTime,
			AgentState: 'Enabled',
			UserAgent: chromeAgent.UserAgent
		}
	}
	return obj;
}

chrome.runtime.onInstalled.addListener((details) => {
	if(details.reason == "install") {
		var obj = genericChromeAgentStore();
		chrome.storage.local.set(obj);
	}
});

chrome.storage.onChanged.addListener(function(changes, namespace) {
	for(key in changes) {
		var storageChange = changes[key];
		var oldValue = {};
		try {
			oldValue.AgentState = storageChange.oldValue.AgentState
		}
		catch {
			oldValue.AgentState = null;
		}
		console.log('Storage key "%s" in namespace "%s" changed. ' +
			'Old value was "%s", new value is "%s".',
			key,
			namespace,
			oldValue.AgentState,
			storageChange.newValue.AgentState);
	}
	window.chromevalue = storageChange.newValue.AgentState;
	chrome.uavalue = storageChange.newValue.UserAgent;
	window.uavalue = storageChange.newValue.UserAgent;
});

chrome.tabs.onCreated.addListener((evt) => {
	chrome.storage.local.get('ChromeAgent', (items) => {
		chrome.uavalue = items.ChromeAgent.UserAgent;
		window.uavalue = items.ChromeAgent.UserAgent;
	});
});